FROM mono:latest
EXPOSE 4321
ADD . /src
WORKDIR /src
RUN apt update && apt install mono-4.0-service -y
CMD [ "mono-service", "/src/HelloWorldService/HelloWorldService.exe" , "--no-daemon" ]

